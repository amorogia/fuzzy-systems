function [error_matrix, OA, PA, UA, k_hat ] = evaluation_metrics(Dchk_out,Dchk_out_pred)
%% Calculate error matrix
error_matrix = confusionmat(Dchk_out,Dchk_out_pred);
error_matrix = transpose(error_matrix);

%% Calculate overall accuracy
% Calculate number of instances in the check dataset
N = sum(sum(error_matrix));
% and then calculate overall accuracy
OA = trace(error_matrix) / N;

%% Calculate producer's and user's accuracy
Xii = diag(error_matrix);
Xr = sum(error_matrix,2);
Xc = transpose(sum(error_matrix));
% Calculate producer's accuracy
PA = Xii./Xc;
% Calculate user's accuracy
UA = Xii./Xr;

%% Calculate k_hat
numerator = (N*trace(error_matrix) - sum(Xr.*Xc));
denominator = (N^2- sum(Xr.*Xc));
k_hat = numerator/denominator;

%% Display Error Matrix
disp('-------------------------------');
disp('ERROR MATRIX ')
disp('-------------------------------');
disp(' ');
disp(error_matrix);

%% Display Evaluation Metrics
disp('-------------------------------');
disp('ACCURACY METRICS')
disp('-------------------------------');
disp(' ');
disp(['OA : ' num2str(OA)]);
disp(['k_hat :  ' num2str(k_hat)]);
disp(' ');
disp('    |Class|  |  PA  |  |  UA  |');
disp([unique(Dchk_out),PA, UA]);
disp(' ');
end

%% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki