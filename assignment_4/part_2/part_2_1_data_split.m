%% Part 2.1 : Partition Dataset - High Dimension Dataset
% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki
clear;

%% Load super conductivity dataset
% Read dataset but skip the first row and first column of data, it is just labels
dataset = csvread('data.csv',1,1);  
% Calculate rows and columns of dataset
rows = size(dataset,1); 
cols = size(dataset,2);

%% Rank Features using relief algorithm
[ranks,~] = relieff(dataset(:,1:(end-1)),dataset(:,end),10);

% Rearrange the order of features in the Datasets based on their ranks
% Initialize ranked data
rankedData = zeros(size(dataset));
% Rearrange Inputs
for column = 1:(cols-1)
    rankedData(:, column) = dataset(:, ranks(column));
end
% Then put output on the last column
rankedData(:,cols) = dataset(:,end);

[Dtrain, Dval, Dchk] = data_split(rankedData);

%% Plot histogram for each data split
figure;
histogram(Dtrain(:,end));
title('Training Data : Histogram');
ylabel('Number of samples');
xlabel('Classes');

figure;
histogram(Dval(:,end));
title('Validation Data : Histogram');
ylabel('Number of samples');
xlabel('Classes');

figure;
histogram(Dchk(:,end));
title('Testing Data : Histogram');
ylabel('Number of samples');
xlabel('Classes');

%% Save all variables from Part 2.1
filename = 'part_2_1_data_split.mat';
save(filename)

%% PARTITIONING DATASET WITH EQUAL FREQUENCY OF CLASSES IN EACH DATASET
% Here we have 2 unique classes
% The partition percentages are : 
% 60% of the dataset : Training data (Dtrain).
% 20% of the dataset : Validation data (Dval).
% 20% of the dataset : Testing data (Dchk).
function [Dtrain, Dval, Dchk] = data_split(dataset)
%% Split dataset to the five classes
split_1 = dataset(dataset(:,end)==1,:);
split_2 = dataset(dataset(:,end)==2,:);
split_3 = dataset(dataset(:,end)==3,:);
split_4 = dataset(dataset(:,end)==4,:);
split_5 = dataset(dataset(:,end)==5,:);

%% Split training, validation and test data of each class
[trainInd_1, valInd_1, chkInd_1] = dividerand(length(split_1), 0.6, 0.2, 0.2);
[trainInd_2, valInd_2, chkInd_2] = dividerand(length(split_2), 0.6, 0.2, 0.2);
[trainInd_3, valInd_3, chkInd_3] = dividerand(length(split_3), 0.6, 0.2, 0.2);
[trainInd_4, valInd_4, chkInd_4] = dividerand(length(split_4), 0.6, 0.2, 0.2);
[trainInd_5, valInd_5, chkInd_5] = dividerand(length(split_5), 0.6, 0.2, 0.2);
%% Merge data 
Dtrain = [split_1(trainInd_1,:); split_2(trainInd_2,:); split_3(trainInd_3,:); split_4(trainInd_4,:); split_5(trainInd_5,:)];
Dval = [split_1(valInd_1,:); split_2(valInd_2,:); split_3(valInd_3,:); split_4(valInd_4,:); split_5(valInd_5,:)];
Dchk = [split_1(chkInd_1,:); split_2(chkInd_2,:); split_3(chkInd_3,:); split_4(chkInd_4,:); split_5(chkInd_5,:)];

%% Shuffle rows of each set 3 times
for i=1:3
Dtrain = Dtrain(randperm(size(Dtrain, 1)), :);
Dval = Dval(randperm(size(Dval, 1)), :);
Dchk = Dchk(randperm(size(Dchk, 1)), :);
end
end