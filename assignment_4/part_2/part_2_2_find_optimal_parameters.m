%% Part 2.2 : High Dimension Dataset - Find optimal parameters
% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki
clear;

%% Load MATLAB enviroment from part 2.1
load('part_2_1_data_split.mat');

%% Grid - Search with Subtractive Clustering and 5 - fold Cross Validation
% We arbitrary specify the number of features values
NF = [5 7 15 21];

% We arbitrary specify the cluster radious values
Ra = [0.25 0.35 0.45 0.71]; 

% Initialize error matrix
e = zeros(length(NF), length(Ra));
% Grid - Search for every combination (NF,Ra) to find optimal no of
% features and cluster radius.
for i=1:length(NF)
    for j = 1:length(Ra)
        e(i,j) = cross_validation(Dtrain,NF(i),Ra(i));
    end
end

%% Find optimal number of features and cluster radius index
% Find the minimum error
minError = min(min(e));
% and then the optimal NF and Ra
[NF_index,Ra_index] = find(e==minError);
NF_optimal = NF(NF_index);
Ra_optimal = Ra(Ra_index);
disp("Best model:");
disp("     NF    Ra");
disp([NF_optimal, Ra_optimal]);

% Plot the error in respect to the number of features
figure;
hold on;
grid on;
for j = 1:4
    plot(NF, e(:, j));
end
title('Mean error respective to # of features');
xlabel('Number of features')
ylabel('RMSE')
legend([num2str(Ra(1)),' radius'], [num2str(Ra(2)),' radius'],...
    [num2str(Ra(3)),' radius'], [num2str(Ra(4)),' radius']);

% Error respective to Influence Radius
figure;
hold on;
grid on;
for i = 1:4
    plot(Ra, e(i, :));
end
title('Mean error respective to Radius');
xlabel('Radius')
ylabel('RMSE')
legend([num2str(NF(1)),' features'], [num2str(NF(2)),' features'],...
    [num2str(NF(3)),' features'], [num2str(NF(4)),' features']);

%% Save all variables from Part 2.2
filename = 'part_2_2_find_optimal_parameters.mat';
save(filename)
