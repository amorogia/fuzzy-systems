%% Part 2.3 : High Dimension Dataset - Train TSK models with optimal parameters
% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki
clear;

%% Load MATLAB enviroment from part 2.1
load('part_2_2_find_optimal_parameters.mat');

%% Separate training data inputs - outputs according to # of features
Dtrain_in = Dtrain(:, 1:NF_optimal);
Dtrain_out = Dtrain(:, end);
Dtrain = [Dtrain_in Dtrain_out];

%% Separate evaluation data inputs - outputs according to # of features
Dval_in = Dval(:, 1:NF_optimal);
Dval_out = Dval(:, end);
Dval = [Dval_in Dval_out];

%% Separate test data inputs - outputs according to # of features
Dchk_in = Dchk(:, 1:NF_optimal);
Dchk_out = Dchk(:, end);
Dchk = [Dchk_in Dchk_out];

%% Find cluster centers and sigma for each cluster
[c1,sig1]=subclust(Dtrain(Dtrain(:,end)==1,:),Ra_optimal);
[c2,sig2]=subclust(Dtrain(Dtrain(:,end)==2,:),Ra_optimal);
[c3,sig3]=subclust(Dtrain(Dtrain(:,end)==3,:),Ra_optimal);
[c4,sig4]=subclust(Dtrain(Dtrain(:,end)==4,:),Ra_optimal);
[c5,sig5]=subclust(Dtrain(Dtrain(:,end)==5,:),Ra_optimal);
        
%% Specify the number of rules
num_rules=size(c1,1)+size(c2,1)+size(c3,1)+size(c4,1)+size(c5,1); 

%% Build FIS From Scratch
initial_fis=newfis('FIS_SC','sugeno');

%% Add Input-Output Variables
name='in';
for i=1:size(Dtrain,2)-1
    a = min(Dtrain(:,i));
    b = max(Dtrain(:,i));
    initial_fis=addvar(initial_fis,'input',name,[a b]);
end
a = min(Dtrain(:,end));
b = max(Dtrain(:,end));
initial_fis=addvar(initial_fis,'output','out1',[a b]);

%% Add Input Membership Functions
name='sth';
for i=1:size(Dtrain,2)-1
    for j=1:size(c1,1)
        initial_fis=addmf(initial_fis,'input',i,name,'gaussmf',[sig1(i) c1(j,i)]);
    end
    for j=1:size(c2,1)
        initial_fis=addmf(initial_fis,'input',i,name,'gaussmf',[sig2(i) c2(j,i)]);
    end
    for j=1:size(c3,1)
        initial_fis=addmf(initial_fis,'input',i,name,'gaussmf',[sig3(i) c3(j,i)]);
    end
    for j=1:size(c4,1)
        initial_fis=addmf(initial_fis,'input',i,name,'gaussmf',[sig4(i) c4(j,i)]);
    end
    for j=1:size(c5,1)
        initial_fis=addmf(initial_fis,'input',i,name,'gaussmf',[sig5(i) c5(j,i)]);
    end
end
        
%% Add Output Membership Functions
params=[ones(1,size(c1,1)) 2*ones(1,size(c2,1)) 3*ones(1,size(c3,1)) 4*ones(1,size(c4,1)) 5*ones(1,size(c5,1))];
for i=1:num_rules
    initial_fis=addmf(initial_fis,'output',1,name,'constant',params(i));
end

%% Add FIS Rule Base
ruleList=zeros(num_rules,size(Dtrain,2));
for i=1:size(ruleList,1)
    ruleList(i,:)=i;
end
ruleList=[ruleList ones(num_rules,2)];
initial_fis=addrule(initial_fis,ruleList);
        
%% Train fis
Epochs = 200;
[trained_fis,trainError, evaluation_fis, chkError]  = train_anfis(initial_fis, Dtrain, Dval, Epochs);

%% Evaluate model
Dchk_out_pred = evalfis(Dchk(:,1:end-1),evaluation_fis);
% Round output
Dchk_out_pred = round(Dchk_out_pred);

%% 1 Plot predictions and real values
figure;
box on; hold on; axis tight;
% Plot actual values
plot(1:length(Dchk_out),Dchk_out,'o');
% Find true positive predictions
TP = Dchk_out == Dchk_out_pred;
% Plot true positives
plot(find(TP == 1),Dchk_out_pred(TP),'+');
% Plot false positives
plot(find(TP == 0),Dchk_out_pred(not(TP)),'*');
title('Original vs Predicted Values');
xlabel('D_{chk} dataset sample');
ylabel('Output');
legend('Original Values','TP Predicted Values','FP Predicted Values');

%% 2. Plot Learning curves
figure;
hold on
title('Learning Curve');
xlabel('Epoch');
ylabel('RMSE Error');
plot(1:Epochs, trainError);
hold on
plot(1:Epochs, chkError);
legend("RMSE_{trn}", "RMSE_{chk}", 'Location', 'north')

%% 3. Plot 2 initial and 2 final fuzzy inputs
for i = 1:2
    random_input = randi([1, NF_optimal]);
    % Plot initial input
    figure;
    box on; hold on;
    plotmf(initial_fis, 'input', random_input)
    title(['Intial input : ', int2str(random_input)])
    xlabel(['Input_{', int2str(random_input), '}']);
    % Plot final input
    figure;
    box on; hold on;
    plotmf(trained_fis, 'input', random_input)
    title(['Trained input : ', int2str(random_input)])
    xlabel(['Input_{', int2str(random_input), '}']);
end

%% 4a. Calculate evaluation metrics 
[error_matrix, OA, PA, UA, k_hat] = evaluation_metrics(Dchk(:,end),Dchk_out_pred);

%% 4b. Plot error matrix
figure;
heatmap(1:5,1:5,error_matrix);
title('Error Matrix');
xlabel('Actual');
ylabel('Predicted');

% Tune Sugeno-type fuzzy inference system using training data
function [trained_fis,trainError, evaluation_fis, chkError]  = train_anfis(initial_fis, trainData, validationData, Epochs)
opt = anfisOptions('InitialFIS',initial_fis);
opt.DisplayANFISInformation = 1;                   
opt.DisplayErrorValues = 0;                        
opt.DisplayStepSize = 0;                           
opt.DisplayFinalResults = 1;                       
opt.EpochNumber = Epochs;                             
opt.OptimizationMethod = 1;                        
opt.ValidationData = validationData;                         
[trained_fis,trainError,~,evaluation_fis,chkError] = anfis(trainData,opt);
end