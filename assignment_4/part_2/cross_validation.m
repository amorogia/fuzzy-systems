%% 5 fold Cross Validation Function 
% For the different number of futures (NF)
% and the different number of radius (Ra)
% we calculate the RMSE Error
function RMSE = cross_validation(dataset, NF, Ra)
    % Cross-Validation with 5 folds. The 5 - fold partitioning separates
    % the dataset with 80% of the data for the training procedure and the
    % other 20% for the validation procedure.
    c = cvpartition(dataset(:,end),'KFold',5,'Stratify',true);
    % Initialize Error for each fold
    e = zeros(c.NumTestSets,1);
    % For each fold calculate average validation error
    for k=1:c.NumTestSets       
        %% Separate training data inputs - outputs according to # of features
        Dtrn_in = dataset(c.training(k), 1:NF);
        Dtrn_out = dataset(c.training(k), end);
        Dtrain = [Dtrn_in Dtrn_out];
        
        %% Separate evaluation data inputs - outputs according to # of features
        Dval_in = dataset(c.test(k), 1:NF);
        Dval_out = dataset(c.test(k), end);
        Dval = [Dval_in Dval_out];
        
        %% Find cluster centers and sigma for each cluster
        [c1,sig1]=subclust(Dtrain(Dtrain(:,end)==1,:),Ra);
        [c2,sig2]=subclust(Dtrain(Dtrain(:,end)==2,:),Ra);
        [c3,sig3]=subclust(Dtrain(Dtrain(:,end)==3,:),Ra);
        [c4,sig4]=subclust(Dtrain(Dtrain(:,end)==4,:),Ra);
        [c5,sig5]=subclust(Dtrain(Dtrain(:,end)==5,:),Ra);
        
        %% Specify the number of rules
        num_rules=size(c1,1)+size(c2,1)+size(c3,1)+size(c4,1)+size(c5,1); 
        
        %% Build FIS From Scratch
        initial_fis=newfis('FIS_SC','sugeno');
        
        %% Add Input-Output Variables
        name='in';
        for i=1:size(Dtrain,2)-1
            a = min(Dtrain(:,i));
            b = max(Dtrain(:,i));
            initial_fis=addvar(initial_fis,'input',name,[a b]);
        end
        a = min(Dtrain(:,end));
        b = max(Dtrain(:,end));
        initial_fis=addvar(initial_fis,'output','out1',[a b]);
        
        %% Add Input Membership Functions
        name='sth';
        for i=1:size(Dtrain,2)-1
            for j=1:size(c1,1)
                initial_fis=addmf(initial_fis,'input',i,name,'gaussmf',[sig1(i) c1(j,i)]);
            end
            for j=1:size(c2,1)
                initial_fis=addmf(initial_fis,'input',i,name,'gaussmf',[sig2(i) c2(j,i)]);
            end
            for j=1:size(c3,1)
                initial_fis=addmf(initial_fis,'input',i,name,'gaussmf',[sig3(i) c3(j,i)]);
            end
            for j=1:size(c4,1)
                initial_fis=addmf(initial_fis,'input',i,name,'gaussmf',[sig4(i) c4(j,i)]);
            end
            for j=1:size(c5,1)
                initial_fis=addmf(initial_fis,'input',i,name,'gaussmf',[sig5(i) c5(j,i)]);
            end
        end
        
        %% Add Output Membership Functions
        params=[ones(1,size(c1,1)) 2*ones(1,size(c2,1)) 3*ones(1,size(c3,1)) 4*ones(1,size(c4,1)) 5*ones(1,size(c5,1))];
        for i=1:num_rules
            initial_fis=addmf(initial_fis,'output',1,name,'constant',params(i));
        end
        
        %% Add FIS Rule Base
        ruleList=zeros(num_rules,size(Dtrain,2));
        for i=1:size(ruleList,1)
            ruleList(i,:)=i;
        end
        ruleList=[ruleList ones(num_rules,2)];
        initial_fis=addrule(initial_fis,ruleList);
    
        %% Train anfis model
        anfis_opt = anfisOptions('InitialFIS', initial_fis, 'EpochNumber', 1, 'ValidationData', Dval);
        anfis_opt.DisplayANFISInformation = 0;
        anfis_opt.DisplayErrorValues = 0;
        anfis_opt.DisplayStepSize = 0;
        anfis_opt.DisplayFinalResults = 1;
        opt.DisplayANFISInformation = 1;                                                            
        opt.OptimizationMethod = 1;
        [~,~,~,~,e(k)] = anfis(Dtrain, anfis_opt);
    end
    % Calculate final error
    RMSE = sum(e)/c.NumTestSets;
end

%% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki