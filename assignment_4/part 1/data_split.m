%% PARTITIONING DATASET WITH EQUAL FREQUENCY OF CLASSES IN EACH DATASET
% Here we have 2 unique classes
% The partition percentages are : 
% 60% of the dataset : Training data (Dtrain).
% 20% of the dataset : Validation data (Dval).
% 20% of the dataset : Testing data (Dchk).
function [Dtrain, Dval, Dchk] = data_split(dataset)
%% Calculate unique classes
unique_class = unique(dataset(:,end));

%% Find index of each class 
clss_1_index = dataset(:,end) == unique_class(1);
clss_2_index = dataset(:,end) == unique_class(2);

%% Split dataset to the two classes
class_1_split_dataset = dataset(clss_1_index,:);
class_2_split_dataset = dataset(clss_2_index,:);

%% Split training, validation and test data of each class
[trainInd_1, valInd_1, chkInd_1] = dividerand(length(class_1_split_dataset), 0.6, 0.2, 0.2);
[trainInd_2, valInd_2, chkInd_2] = dividerand(length(class_2_split_dataset), 0.6, 0.2, 0.2);

%% Merge data 
Dtrain = [class_1_split_dataset(trainInd_1,:); class_2_split_dataset(trainInd_2,:)];
Dval = [class_1_split_dataset(valInd_1,:); class_2_split_dataset(valInd_2,:)];
Dchk = [class_1_split_dataset(chkInd_1,:); class_2_split_dataset(chkInd_2,:)];

%% Shuffle rows of each set
Dtrain = Dtrain(randperm(size(Dtrain, 1)), :);
Dval = Dval(randperm(size(Dval, 1)), :);
Dchk = Dchk(randperm(size(Dchk, 1)), :);
end

%% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki