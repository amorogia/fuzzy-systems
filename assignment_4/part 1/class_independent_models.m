%% Part 1.2.1 : Simple Dataset - Train class independent TSK models
% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki
clear;

%% LOAD HABERMAN DATASET
% INPUTS:
% 1. Age of patient at time of operation (numerical)
% 2. Patient's year of operation (year - 1900, numerical)
% 3. Number of positive axillary nodes detected (numerical) 
% 
% OUTPUT:
% 4. Survival status (class attribute)
% -- 1 = the patient survived 5 years or longer
% -- 2 = the patient died within 5 year
dataset = load('dataset/haberman.data');

%% PARTITIONING DATASET
[Dtrain, Dval, Dchk] = data_split(dataset);

%% Plot histogram for each data split
figure;
histogram(Dtrain(:,end));
title('Training Data : Histogram');
ylabel('Number of samples');
xlabel('Classes');

figure;
histogram(Dval(:,end));
title('Validation Data : Histogram');
ylabel('Number of samples');
xlabel('Classes');

figure;
histogram(Dchk(:,end));
title('Testing Data : Histogram');
ylabel('Number of samples');
xlabel('Classes');

%% Define Cluster radius 
Ra = [0.258 , 0.9];

for i=1:2
%% Generate Class-Independent initial .fis
initial_fis = fis_gen(Dtrain(:,1:end-1), Dtrain(:,end), Ra(i));

% Make singleton outputs
for mf_output_number = 1:length(initial_fis.output.mf)
    initial_fis.output.mf(mf_output_number).type = 'constant';
    initial_fis.output.mf(mf_output_number).params = mf_output_number;
end

%% Train fis
Epochs = 100;
[trained_fis,trainError, evaluation_fis, chkError]  = train_anfis(initial_fis, Dtrain, Dval, Epochs);

%% Evaluate model
Dchk_out_pred = evalfis(Dchk(:,1:end-1),evaluation_fis);
Dchk_out_pred = round(Dchk_out_pred);

%% 1. Plot final fuzzy inputs
for input_number = 1:3
    % Plot final input
    figure;
    box on; hold on;
    plotmf(evaluation_fis, 'input', input_number)
    title(['Trained input : ', int2str(input_number)])
    xlabel(['Input_{', int2str(input_number), '}']);
end

%% 2. Plot Learning curves
figure;
hold on
title('Learning Curve');
xlabel('Epoch');
ylabel('RMSE Error');
plot(1:Epochs, trainError);
hold on
plot(1:Epochs, chkError);
legend("RMSE_{trn}", "RMSE_{chk}", 'Location', 'north')  

%% 3. Calculate evaluation metrics 
[error_matrix, OA, PA, UA, k_hat] = evaluation_metrics(Dchk(:,end),Dchk_out_pred);
end

%% Function to generate TSK fuzzy inference system object from data
% INPUTS : 
% input_data = input data to generate fuzzy system
% output_data = output data to generate fuzzy system
% Ra = 
%
% OUTPUT:
% FIS = fuzzy inference system object
function FIS = fis_gen(inputData, outputData, Ra)
% Specify .fis options. 
options = genfisOptions('SubtractiveClustering');  
options.ClusterInfluenceRange = Ra;  
FIS = genfis(inputData, outputData, options); 
end

% Tune Sugeno-type fuzzy inference system using training data
function [trained_fis,trainError, evaluation_fis, chkError]  = train_anfis(initial_fis, trainData, validationData, Epochs)
opt = anfisOptions('InitialFIS',initial_fis);
opt.DisplayANFISInformation = 1;                   
opt.DisplayErrorValues = 0;                        
opt.DisplayStepSize = 0;                           
opt.DisplayFinalResults = 1;                       
opt.EpochNumber = Epochs;                             
opt.OptimizationMethod = 1;                        
opt.ValidationData = validationData;                         
[trained_fis,trainError,~,evaluation_fis,chkError] = anfis(trainData,opt);
end