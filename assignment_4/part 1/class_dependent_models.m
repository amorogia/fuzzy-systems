%% Part 1.2.2 : Simple Dataset - Train class dependent TSK models
% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki
clear;

%% LOAD HABERMAN DATASET
% INPUTS:
% 1. Age of patient at time of operation (numerical)
% 2. Patient's year of operation (year - 1900, numerical)
% 3. Number of positive axillary nodes detected (numerical) 
% 
% OUTPUT:
% 4. Survival status (class attribute)
% -- 1 = the patient survived 5 years or longer
% -- 2 = the patient died within 5 year
dataset = load('dataset/haberman.data');

%% PARTITIONING DATASET
[Dtrain, Dval, Dchk] = data_split(dataset);

%% Plot histogram for each data split
figure;
histogram(Dtrain(:,end));
title('Training Data : Histogram');
ylabel('Number of samples');
xlabel('Classes');

figure;
histogram(Dval(:,end));
title('Validation Data : Histogram');
ylabel('Number of samples');
xlabel('Classes');

figure;
histogram(Dchk(:,end));
title('Testing Data : Histogram');
ylabel('Number of samples');
xlabel('Classes');

%% Define Cluster radius 
Ra = [0.258 , 0.9];

for radius = 1:2
[c1,sig1]=subclust(Dtrain(Dtrain(:,end)==1,:),Ra(radius));
[c2,sig2]=subclust(Dtrain(Dtrain(:,end)==2,:),Ra(radius));
num_rules=size(c1,1)+size(c2,1); 
 
%Build FIS From Scratch
initial_fis=newfis('FIS_SC','sugeno');

%Add Input-Output Variables
names_in={'in1','in2','in3'};
for i=1:size(Dtrain,2)-1
    a = min(Dtrain(:,i));
    b = max(Dtrain(:,i));
    initial_fis=addvar(initial_fis,'input',names_in{i},[a b]);
end
a = min(Dtrain(:,end));
b = max(Dtrain(:,end));
initial_fis=addvar(initial_fis,'output','out1',[a b]);

%Add Input Membership Functions
name='sth';
for i=1:size(Dtrain,2)-1
    for j=1:size(c1,1)
        initial_fis=addmf(initial_fis,'input',i,name,'gaussmf',[sig1(i) c1(j,i)]);
    end
    for j=1:size(c2,1)
        initial_fis=addmf(initial_fis,'input',i,name,'gaussmf',[sig2(i) c2(j,i)]);
    end
end

%Add Output Membership Functions
params=[ones(1,size(c1,1)) 2*ones(1,size(c2,1))];
for i=1:num_rules
    initial_fis=addmf(initial_fis,'output',1,name,'constant',params(i));
end

%Add FIS Rule Base
ruleList=zeros(num_rules,size(Dtrain,2));
for i=1:size(ruleList,1)
    ruleList(i,:)=i;
end
ruleList=[ruleList ones(num_rules,2)];
initial_fis=addrule(initial_fis,ruleList);

%% Train fis
Epochs = 150;
[trained_fis,trainError, evaluation_fis, chkError]  = train_anfis(initial_fis, Dtrain, Dval, Epochs);

%% Evaluate model
Dchk_out_pred = evalfis(Dchk(:,1:end-1),evaluation_fis);
Dchk_out_pred = round(Dchk_out_pred);

%% 1. Plot 2 final fuzzy inputs
for input = 1:3
    % Plot final input
    figure;
    box on; hold on;
    plotmf(evaluation_fis, 'input', input)
    title(['Trained input : ', int2str(input)])
    xlabel(['Input_{', int2str(input), '}']);
end

%% 2. Plot Learning curves
figure;
hold on
title('Learning Curve');
xlabel('Epoch');
ylabel('RMSE Error');
plot(1:Epochs, trainError);
hold on
plot(1:Epochs, chkError);
legend("RMSE_{trn}", "RMSE_{chk}", 'Location', 'north')  

%% 3. Calculate evaluation metrics 
[error_matrix, OA, PA, UA, k_hat] = evaluation_metrics(Dchk(:,end),Dchk_out_pred);

end

% Tune Sugeno-type fuzzy inference system using training data
function [trained_fis,trainError, evaluation_fis, chkError]  = train_anfis(initial_fis, trainData, validationData, Epochs)
opt = anfisOptions('InitialFIS',initial_fis);
opt.DisplayANFISInformation = 1;                   
opt.DisplayErrorValues = 0;                        
opt.DisplayStepSize = 0;                           
opt.DisplayFinalResults = 1;                       
opt.EpochNumber = Epochs;                             
opt.OptimizationMethod = 1;                        
opt.ValidationData = validationData;                         
[trained_fis,trainError,~,evaluation_fis,chkError] = anfis(trainData,opt);
end