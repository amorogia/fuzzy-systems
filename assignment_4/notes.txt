 class_dependent_models
 
  MODEL 1
 
 ANFIS info: 
	Number of nodes: 150
	Number of linear parameters: 18
	Number of nonlinear parameters: 108
	Total number of parameters: 126
	Number of training data pairs: 184
	Number of checking data pairs: 61
	Number of fuzzy rules: 18

Minimal training RMSE = 0.347517
Minimal checking RMSE = 0.467531
-------------------------------
ERROR MATRIX 
-------------------------------
 
    38    12
     7     4

-------------------------------
ACCURACY METRICS
-------------------------------
 
OA : 0.68852
k_hat :  0.10502
 
    |Class|  |  PA  |  |  UA  |
    1.0000    0.8444    0.7600
    2.0000    0.2500    0.3636

  MODEL 2

ANFIS info: 
	Number of nodes: 22
	Number of linear parameters: 2
	Number of nonlinear parameters: 12
	Total number of parameters: 14
	Number of training data pairs: 184
	Number of checking data pairs: 61
	Number of fuzzy rules: 2

Minimal training RMSE = 0.400474
Minimal checking RMSE = 0.44286
-------------------------------
ERROR MATRIX 
-------------------------------
 
    42    11
     3     5

-------------------------------
ACCURACY METRICS
-------------------------------
 
OA : 0.77049
k_hat :  0.29305
 
    |Class|  |  PA  |  |  UA  |
    1.0000    0.9333    0.7925
    2.0000    0.3125    0.6250

 
 
 
 MODEL 3
 
 
 ANFIS info: 
	Number of nodes: 302
	Number of linear parameters: 37
	Number of nonlinear parameters: 222
	Total number of parameters: 259
	Number of training data pairs: 184
	Number of checking data pairs: 61
	Number of fuzzy rules: 37

Warning: number of data is smaller than number of modifiable parameters
Minimal training RMSE = 0.270145
Minimal checking RMSE = 0.486549
-------------------------------
ERROR MATRIX 
-------------------------------
 
    42    11
     3     5
-------------------------------
ACCURACY METRICS
-------------------------------
 
OA : 0.77049
k_hat :  0.29305
 
    |Class|  |  PA  |  |  UA  |
    1.0000    0.9333    0.7925
    2.0000    0.3125    0.6250
    
MODEL 4

ANFIS info: 
	Number of nodes: 38
	Number of linear parameters: 4
	Number of nonlinear parameters: 24
	Total number of parameters: 28
	Number of training data pairs: 184
	Number of checking data pairs: 61
	Number of fuzzy rules: 4

Minimal training RMSE = 0.380044
Minimal checking RMSE = 0.430248
-------------------------------
ERROR MATRIX 
-------------------------------
 
    40    15
     5     1
-------------------------------
ACCURACY METRICS
-------------------------------
 
OA : 0.67213
k_hat :  -0.06087
 
    |Class|  |  PA  |  |  UA  |
    1.0000    0.8889    0.7273
    2.0000    0.0625    0.1667

    
MULTIDIMENSIONAL

OPTIMAL
    NF    Ra
   21.0000    0.4500

-------------------------------
ERROR MATRIX 
-------------------------------
 
   292    11     1     0     0
   101    18     9    17     0
    64   256   269   272   173
     3   175   181   170   287
     0     0     0     1     0

-------------------------------

-------------------------------
ACCURACY METRICS
-------------------------------
 
OA : 0.32565
k_hat :  0.15707
 
    |Class|  |  PA  |  |  UA  |
    1.0000    0.6348    0.9605
    2.0000    0.0391    0.1241
    3.0000    0.5848    0.2602
    4.0000    0.3696    0.2083
    5.0000         0         0
