%% Part 2 - Scenario 1 : Fine tune FZ - PI Controller
% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki

% Get linear_PI variables from script linear_PI
linear_PI;

%% Initial FZ -PI 
%%% Initial Fuzzy PI Parameters
Ti = Kp/Ki;
a = Ti;
Ke = 1;
Kd = a*Ke;
if (a*Ke<1)
    K1 = Kp/(a*Ke);
else
    K1 = Kp;
end

%%% Initial Fuzzy PI simulink system
sim('FLC_System_Initial'); % Run system
% Plot FZ - PI, Initial step response and print step info.
figure;
plot(FZ_PI_Initial.Time,FZ_PI_Initial.Data);
title('Initial PI Step Response');
xlabel('Time (seconds)');
ylabel('Amplitude'); 
% Print FZ - PI step response info.
FZ_PI_Initial_info = stepinfo(FZ_PI_Initial.Data,FZ_PI_Initial.Time)

%% Final FZ -PI 
% Final PI parameters
% After tuning a_final, Ke_final and K1_final variables
a_final = 0.25;
Ke_final = 1.1;
Kd_final = a_final*Ke_final;
K1_final = 20;

%%% Final Fuzzy PI simulink system
sim('FLC_System_Final');% Run system
% Plot FZ - PI, Final step response and print step info.
figure;
plot(FZ_PI_Final.Time,FZ_PI_Final.Data);
title('Final PI Step Response');
xlabel('Time (seconds)');
ylabel('Amplitude'); 
% Print FZ - PI step response info.
FZ_PI_Final_info = stepinfo(FZ_PI_Final.Data,FZ_PI_Final.Time)

%% Evaluate .fis system
%  Load FLC
fis = readfis('satelite_flc');
% We need e = 'NM' and de = 'ZR'
e = -0.5;
De = 0;
input = [e ; De];
[output,fuzzifiedIn,ruleOut,aggregatedOut,ruleFiring] = evalfis(input,fis);
outputRange = linspace(fis.output.range(1),fis.output.range(2),length(aggregatedOut))'; 
figure;
plot(outputRange,aggregatedOut,[output output],[0 1]);