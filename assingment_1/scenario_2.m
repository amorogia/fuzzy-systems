%% Part 3 - Scenario 2 : Response in different r(t) profiles
% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki
clear all;

%% Profile 1 : r(t) = 60*u(t) -40*(t-4) +20*u(t-8)
sim('FLC_System_Final_scenario_2_1');

% Plot FZ - PI, response.
figure;
plot(FZ_PI_Final_Input_scenario_2_1.Time,FZ_PI_Final_Input_scenario_2_1.Data);
hold on;
plot(FZ_PI_Final_Output_scenario_2_1.Time,FZ_PI_Final_Output_scenario_2_1.Data);
title('Profile 1 : Response');
xlabel('Time (seconds)');
ylabel('Amplitude'); 

%% Profile 2 : ramp function
sim('FLC_System_Final_scenario_2_2');

% Plot FZ - PI, response.
figure;
plot(FZ_PI_Final_Input_scenario_2_2.Time,FZ_PI_Final_Input_scenario_2_2.Data);
hold on;
plot(FZ_PI_Final_Output_scenario_2_2.Time,FZ_PI_Final_Output_scenario_2_2.Data);
title('Profile 2 : Response');
xlabel('Time (seconds)');
ylabel('Amplitude'); 