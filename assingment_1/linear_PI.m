%% Part 1 : Linear Controler Design
% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki
clear all

%% Define transfer function
K = 30;                          
c = 1.1;     
s = tf('s');
tf = K*(s+c)/(s*(s+1)*(s+9));  
%% Plot root locus diagram
rlocus(tf);  
%% Plot and print step response info (60*u(t))
figure;
step(feedback(tf, 1, -1),0 : 0.01: 10,stepDataOptions('StepAmplitude',60))
% Print step response info
closed_loop = feedback(tf, 1, -1);
Linear_PI_info = stepinfo(closed_loop)
%% Calculate proportional and integral gains of the PI controller
Kp = K/10;
Ki = c*Kp;

