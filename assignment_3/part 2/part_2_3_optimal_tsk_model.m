%% Part 2.3 : High Dimension Dataset - Train Model with optimal parameters
% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki
clear;

%% Load MATLAB enviroment from part 2.2
load('part_2_2_optimal_parameters.mat');

%% Seperate Dataset according to optimal number of features
% Separate training data inputs and outputs according to the number of
% features
Dtrn_in = Dtrn(:, 1:NF_optimal);
Dtrn_out = Dtrn(:, end);
Dtrn = [Dtrn_in Dtrn_out];
% Separate evaluation data inputs and outputs according to the number of
% features
Dval_in = Dval(:, 1:NF_optimal);
Dval_out = Dval(:, end);
Dval = [Dval_in Dval_out];
% Separate test data inputs and outputs according to the number of
% features
Dchk_in = Dchk(:, 1:NF_optimal);
Dchk_out = Dchk(:, end);
Dchk = [Dchk_in Dchk_out];

%% Initialize fis
initial_fis = fis_gen(Dtrn_in, Dtrn_out, Ra_optimal);

%% Train anfis model
Epochs  = 80;
[trained_fis,trainError, evaluation_fis, chkError] = train_anfis(initial_fis, Dtrn, Dval, Epochs);
% Save trained_fis
writefis(evaluation_fis,'TSK_model_with_optimal_parameters');

%% Evaluate model
Dchk_out_pred = evalfis(Dchk_in,evaluation_fis);

%% Evaluation metrics
% Calculate MSE and RMSE
MSE = mean((Dchk_out - Dchk_out_pred).^2);
RMSE = sqrt(MSE);
% Calculate R^2
SSres = sum((Dchk_out - Dchk_out_pred).^2);
SStot = sum((Dchk_out - mean(Dchk_out)).^2);
R2 = 1 - (SSres/SStot);
% Calculate NMSE and NDEI
NMSE = SSres/SStot;
NDEI = sqrt(NMSE);

%% Display Metrics
disp('Model Evaluation Indexes')
disp(['  MSE: ' num2str(MSE)])
disp(['  RMSE: ' num2str(RMSE)])
disp(['  NMSE: ' num2str(NMSE)])
disp(['  NDEI: ' num2str(NDEI)])
disp(['  R^2:  ' num2str(R2)])

%% 1. Plot predictions and real values
figure;
box on; hold on; axis tight;
plot(Dchk_out_pred);
plot(Dchk_out);
title('Original vs Predicted Values');
xlabel('D_{chk} dataset sample');
ylabel('Output');
legend('Predicted Values', 'Original Values');

%% 2a. Plot Learning curves
figure;
hold on
title('Learning Curve');
xlabel('Epoch');
ylabel('RMSE Error');
plot(1:Epochs, trainError);
hold on
plot(1:Epochs, chkError);
legend("RMSE_{trn}", "RMSE_{chk}", 'Location', 'north')  

%% 2b. Plot prediction squared error
figure;
hold on
title('Prediction Error');
xlabel('Testing Dataset Sample');
ylabel('Squared Error');
plot(1:length(Dchk_out_pred), (Dchk_out_pred - Dchk_out).^2 );

%% 3. Plot 2 initial and 2 final fuzzy inputs
for i = 1:2
    random_input = randi([1, NF_optimal]);
    % Plot initial input
    figure;
    box on; hold on;
    plotmf(initial_fis, 'input', random_input)
    title(['Intial input : ', int2str(random_input)])
    xlabel(['Input_{', int2str(random_input), '}']);
    % Plot final input
    figure;
    box on; hold on;
    plotmf(trained_fis, 'input', random_input)
    title(['Trained input : ', int2str(random_input)])
    xlabel(['Input_{', int2str(random_input), '}']);
end

%% Function to generate TSK fuzzy inference system object from data
% INPUTS : 
% input_data = input data to generate fuzzy system
% output_data = output data to generate fuzzy system
% Ra = 
%
% OUTPUT:
% FIS = fuzzy inference system object
function FIS = fis_gen(inputData, outputData, Ra)
% Specify .fis options. 
options = genfisOptions('SubtractiveClustering');  
options.ClusterInfluenceRange = Ra;
FIS = genfis(inputData, outputData, options); 
end

%% Function to Tune Sugeno-type fuzzy inference system using training data
function [trained_fis,trainError, evaluation_fis, chkError]  = train_anfis(initial_fis, trainData, validationData, Epochs)
opt = anfisOptions('InitialFIS',initial_fis);
opt.DisplayANFISInformation = 1;                   
opt.DisplayErrorValues = 0;                        
opt.DisplayStepSize = 0;                           
opt.DisplayFinalResults = 1;                       
opt.EpochNumber = Epochs;                             
opt.OptimizationMethod = 1;                        
opt.ValidationData = validationData;                         
[trained_fis,trainError,~,evaluation_fis,chkError] = anfis(trainData,opt);
end
