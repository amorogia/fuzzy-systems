%% 5 fold Cross Validation Function 
% For the different number of futures (NF)
% and the different number of radius (Ra)
% we calculate the RMSE Error
function RMSE = cross_validation(dataset, NF, Ra)
    % Cross-Validation with 5 folds. The 5 - fold partitioning separates
    % the dataset with 80% of the data for the training procedure and the
    % other 20% for the validation procedure.
    c = cvpartition(dataset(:,end),'KFold',5);
    % Initialize Error for each fold
    e = zeros(c.NumTestSets,1);
    % For each fold calculate average validation error
    for k=1:c.NumTestSets       
        % Separate training data inputs and outputs according to the number of
        % features
        Dtrn_in = dataset(c.training(k), 1:NF);
        Dtrn_out = dataset(c.training(k), end);
        Dtrn = [Dtrn_in Dtrn_out];
        % Separate evaluation data inputs and outputs according to the number of
        % features
        Dval_in = dataset(c.test(k), 1:NF);
        Dval_out = dataset(c.test(k), end);
        Dval = [Dval_in Dval_out];
        
        % Initialize fis 
        fis_opt = genfisOptions('SubtractiveClustering');
        fis_opt.ClusterInfluenceRange = Ra;
        initial_fis = genfis(dataset(:,1:NF), dataset(:,end), fis_opt);
        
        % Train anfis model
        anfis_opt = anfisOptions('InitialFIS', initial_fis, 'EpochNumber', 1, 'ValidationData', Dval);
        anfis_opt.DisplayANFISInformation = 0;
        anfis_opt.DisplayErrorValues = 0;
        anfis_opt.DisplayStepSize = 0;
        anfis_opt.DisplayFinalResults = 1;
        opt.DisplayANFISInformation = 1;                                                            
        opt.OptimizationMethod = 1;
        [~,~,~,~,e(k)] = anfis(Dtrn, anfis_opt);
    end
    % Calculate final error
    RMSE = sum(e)/c.NumTestSets;
end

%% Student Info 
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki