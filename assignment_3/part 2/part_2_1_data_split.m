%% Part 2.1 : Partition Dataset - High Dimension Dataset
% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki
clear;

%% Load super conductivity dataset
% Read dataset but skip the first row of data, it is just labels
dataset = csvread('train.csv',1,0);  
% Calculate rows and columns of dataset
rows = size(dataset,1); 
cols = size(dataset,2);

%% Rank Features using relief algorithm
[ranks,~] = relieff(dataset(:,1:(end-1)),dataset(:,end),10);

% Rearrange the order of features in the Datasets based on their ranks
% Initialize ranked data
rankedData = zeros(size(dataset));
% Rearrange Inputs
for column = 1:(cols-1)
    rankedData(:, column) = dataset(:, ranks(column));
end
% Then put output on the last column
rankedData(:,cols) = dataset(:,end);

%% Split data
[trainInd, valInd, chkInd] = dividerand(length(rankedData), 0.6, 0.2, 0.2);
Dtrn = rankedData(trainInd,:);
Dval = rankedData(valInd,:);
Dchk = rankedData(chkInd,:);

%% Save all variables from Part 2.1
filename = 'part_2_1_data_split.mat';
save(filename)