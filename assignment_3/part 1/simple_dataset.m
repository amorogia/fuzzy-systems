%% Part 1 : Simple dataset
% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki

%% LOAD AIRFOIL SELF NOISE DATASET
% INPUTS:
% 1. Frequency, in Hertzs.
% 2. Angle of attack, in degrees.
% 3. Chord length, in meters.
% 4. Free-stream velocity, in meters per second.
% 5. Suction side displacement thickness, in meters.
% 
% OUTPUT:
% 6. Scaled sound pressure level, in decibels.
dataset = load('airfoil_self_noise.dat');
 
%% PARTITIONING DATASET
% The partition percentages are : 
% 60% of the dataset : Training data.
% 20% of the dataset : Validation data.
% 20% of the dataset : Testing data.
[trainInd, valInd, chkInd] = dividerand(length(dataset), 0.6, 0.2, 0.2);
Dtrain = dataset(trainInd,:);
Dval = dataset(valInd,:);
Dchk = dataset(chkInd,:);

%% Define different TSK model parameters
numMFs = [2; 3; 2; 3];
outputTypes = ["constant"; "constant"; "linear"; "linear"];
Epochs  = 150;

for i=1:4
   % Print number of TSK Model
    disp(['Model : ',num2str(i)]);
    disp(['Number of membership functions : ',num2str(numMFs(i))]);
    disp(['Output Type : ',outputTypes(i)]);
    
    %% Generate initial fis
    initial_fis = fis_gen(Dtrain(:,1:5), Dtrain(:, 6), numMFs(i), outputTypes(i));
    %% Train / tune fis
    [trained_fis,trainError, evaluation_fis, chkError] = train_anfis(initial_fis, Dtrain, Dval, Epochs);
    % Save trained_fis
    writefis(trained_fis,['TSK_model_',num2str(i)]);
    
    %% Plot MFs
    figure;
    plotmf(evaluation_fis,'input',1)
    title('Input 1 : Frequency');
    xlabel('Hz');
    
    figure;
    plotmf(evaluation_fis,'input',2)
    title('Input 2 : Angle of attack');
    xlabel('degrees');
    
    figure;
    plotmf(evaluation_fis,'input',3)
    title('Input 3 : Chord length');
    xlabel('Meters (m)');
    
    figure;
    plotmf(evaluation_fis,'input',4)
    title('Input 4 : Free-stream velocity'); 
    xlabel('Meters (m/s)');

    figure;
    plotmf(evaluation_fis,'input',5)
    title('Input 5 : Suction side displacement thickness'); 
    xlabel('Meters (m)');
    
    %% Learning curves
    figure;
    hold on
    title('Learning Curve');
    xlabel('Epoch');
    ylabel('RMSE Error');
    plot(1:Epochs, trainError);
    hold on
    plot(1:Epochs, chkError);
    legend("RMSE_{trn}", "RMSE_{chk}", 'Location', 'north')  
    
    % Specify inputs and outputs for test set
    chk_input = Dchk(:,1:5);
    chk_output = Dchk(:,6);
    % Calculate trained fis output
    pred_output = evalfis(chk_input,evaluation_fis);
    
    %% Plot prediction squared error
    figure;
    hold on
    title('Prediction Error');
    xlabel('Testing Dataset Sample');
    ylabel('Squared Error');
    plot(1:length(pred_output), (pred_output - chk_output).^2 );
    
    %% Evaluation metrics
    % Calculate MSE and RMSE
    MSE = mean((chk_output - pred_output).^2);
    RMSE = sqrt(MSE);
    % Calculate R^2
    SSres = sum((chk_output - pred_output).^2);
    SStot = sum((chk_output - mean(chk_output)).^2);
    R2 = 1 - (SSres/SStot);
    % Calculate NMSE and NDEI
    NMSE = SSres/SStot;
    NDEI = sqrt(NMSE);
    
    %% Display Metrics
    disp('Model Evaluation Indexes')
    disp(['  MSE: ' num2str(MSE)])
    disp(['  RMSE: ' num2str(RMSE)])
    disp(['  NMSE: ' num2str(NMSE)])
    disp(['  NDEI: ' num2str(NDEI)])
    disp(['  R^2:  ' num2str(R2)])
end

%% Function to generate TSK fuzzy inference system object from data
% INPUTS : 
% input_data = input data to generate fuzzy system
% output_data = output data to generate fuzzy system
% no_mfs = Number of membership functions for each input

% output_type =  Output membership function type
%    'linear' — The output of each rule is a linear function of the input variables, scaled by the antecedent result value.
%    'constant' — The output of each rule is a constant, scaled by the antecedent result value.
%
% OUTPUT:
% FIS = fuzzy inference system object
function FIS = fis_gen(inputData, outputData, numMFs, outputType)
% Specify .fis options. 
options = genfisOptions('GridPartition');  
% Specify number of membership functions.
options.NumMembershipFunctions = numMFs;  
% Use bell membership functions.
options.InputMembershipFunctionType = 'gbellmf';    
% Type of output singleton or polynomial.
options.OutputMembershipFunctionType = outputType;  
% Finally generate system.
FIS = genfis(inputData, outputData, options); 
end

%% Function to Tune Sugeno-type fuzzy inference system using training data
function [trained_fis,trainError, evaluation_fis, chkError]  = train_anfis(initial_fis, trainData, validationData, Epochs)
opt = anfisOptions('InitialFIS',initial_fis);
opt.DisplayANFISInformation = 1;                   
opt.DisplayErrorValues = 0;                        
opt.DisplayStepSize = 0;                           
opt.DisplayFinalResults = 1;                       
opt.EpochNumber = Epochs;                             
opt.OptimizationMethod = 1;                        
opt.ValidationData = validationData;                         
[trained_fis,trainError,~,evaluation_fis,chkError] = anfis(trainData,opt);
end
