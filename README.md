# Υπολογιστική Νοημοσύνη

Εργασίες του μαθήματος "Ασαφή Συστήματα / Υπολογιστική Νοημοσύνη", όπως αυτές δόθηκαν κατά το εαρινό εξάμηνο του 2020.

## Δομή φακέλων του παραδοτέου

Οι 4εις εργασίες έχουν χωριστεί σε φακέλους με όνομα : `assignment_*`. 
Κάθε φάκελος περιέχει τα πηγαία MATLAB αρχεία και τα παράγωγά τους καθώς και την αναφορά με τα πηγαία latex.
Πιο συγκεκριμένα : 

Τα αρχεία MATLAB βρίσκονται στο φάκελο :
`./assignment_*/`

Τα αρχεία latex βρίσκονται στο φάκελο :
`assignment_*/ass-*_latex_report/`

...και η compiled αναφορά για κάθε εργασία : 
`assignment_*/ass-*_latex_report/main.pdf`

`Όπου * ο αριθμός της κάθε εργασίας (1, 2, 3, 4)`

### Εργασία 1 (./assingment_1/)
FLC 15
- Αρχείο Εκφώνησης : `15_Satellite.pdf`
- Αρχείο αναφοράς : `./ass-1_latex_report/main.pdf`

#### Παραδοτέα MATLAB (./assingment_1/)
- linear_PI.m      
- scenario_1.m      
- FLC_System_Initial.slx
- FLC_System_Final.slx 
- scenario_2.m
- FLC_System_Final_scenario_2_1.slx 
- FLC_System_Final_scenario_2_2.slx  
- satelite_flc.fis  

### Εργασία 2 (./assingment_2)
Car Control H
- Αρχείο Εκφώνησης : `H_CarControl.pdf`
- Αρχείο αναφοράς : `./ass-2_latex_report/main.pdf`

#### Παραδοτέα MATLAB (./assingment_2/)
- car_flc_initial.fis  
- self_driving_simulation_initial.slx
- car_flc_final.fis    
- self_driving_simulation_final.slx
- plot_route.m      
- map.mat           

### Εργασία 3 (./assingment_3)
- Αρχείο Εκφώνησης : `Regression.pdf`
- Αρχείο αναφοράς : `./ass-3_latex_report/main.pdf`

#### Παραδοτέα MATLAB (./assingment_3)
##### Μέρος 1 (./assingment_3/part_1)
- simple_dataset.m        
- airfoil_self_noise.dat  
- TSK_model_1.fis  
- TSK_model_2.fis  
- TSK_model_3.fis
- TSK_model_4.fis

##### Μέρος 2 (./assingment_3/part_2)
- part_2_1_data_split.m    
- part_2_1_data_split.mat  
- part_2_2_optimal_parameters.m    
- part_2_2_optimal_parameters.mat  
- part_2_3_optimal_tsk_model.m     
- cross_validation.m       
- train.csv
- TSK_model_with_optimal_parameters.fis

### Εργασία 4 (./assingment_4)
- Αρχείο Εκφώνησης : `Classification.pdf`
- Αρχείο αναφοράς : `./ass-4_latex_report/main.pdf`

#### Παραδοτέα MATLAB (./assingment_4)
##### Μέρος 1 (./assingment_4/part_1)
- data_split.m  
- class_dependent_models.m    
- class_independent_models.m  
- evaluation_metrics.m

##### Μέρος 2 (./assingment_4/part_2)
- part_2_1_data_split.m               
- part_2_1_data_split.mat             
- part_2_2_find_optimal_parameters.m  
- part_2_2_find_optimal_parameters.mat
- part_2_3_train_optimal_tsk.m
- cross_validation.m    
- evaluation_metrics.m  
- data.csv              

Αναλυτικότερα στις αναφορές κάθε εργασίας `assignment_*/ass-*_latex_report/main.pdf`
