%% Run self driving system and plot routes for initial and final controllers
% Student Info :
% Name : Apostolos Morogiannis 
% AEM : 8956
% email : amorogia@ece.auth.gr
% Aristotle University of Thessaloniki

sim('self_driving_simulation_initial');
sim('self_driving_simulation_final');

draw_map();
% Plot Initial Car Route
plot(car_route_initial.Data(:,1),car_route_initial.Data(:,2),'LineWidth',2);
% Change title
title('Car route : Initial Contoller')
legend('Initial Possition','Desired Position','Route');

draw_map();
% Plot Final Car Route
plot(car_route_final.Data(:,1),car_route_final.Data(:,2),'LineWidth',2);
% Change title
title('Car route : Final Contoller')
legend('Initial Possition','Desired Position','Route');

% Define desired position
x_desired = 15;
y_desired = 7.2;

% Calculate initial error
x_error_initial = abs(car_route_initial.Data(end,1) - 15);
y_error_initial = abs(car_route_initial.Data(end,2) - 7.2);
% Display initial errors
disp(' ');
disp('-------------------------')
disp('Initial Controller Errors');
disp('-------------------------')
disp(' ');
disp(['x_error_initial = ', num2str(x_error_initial)]);
disp(['y_error_initial = ', num2str(y_error_initial)]);

% Calculate final error
x_error_final = abs(car_route_final.Data(end,1) - 15);
y_error_final = abs(car_route_final.Data(end,2) - 7.2);
% Display final errors
disp(' ');
disp('-----------------------')
disp('Final Controller Errors');
disp('-----------------------')
disp(' ');
disp(['x_error_final = ', num2str(x_error_final)]);
disp(['y_error_final = ', num2str(y_error_final)]);

function draw_map()
figure;
hold on;
% Load and plot occupancy grid map from map.mat
load('map.mat','map');
show(map);
% Specify initial position (x_init,y_init) = (9, 4.4)
scatter(9,4.4,25,'r','+')
% Specify desired position (xd,yd) = (15, 7.2)
scatter(15,7.2,25,'g','o')
end